package com.hcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author hepangui
 * @date 2018年10月26日
 * 网关
 */
@SpringBootApplication
@EnableCaching
public class HCloudGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(HCloudGatewayApplication.class, args);
    }
}
