package com.hcloud.audit.operate.entity;

import com.hcloud.common.crud.entity.BaseEntity;
import com.hcloud.common.crud.entity.BaseTreeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Data
@Entity
@Table(name = "h_audit_opreate_log",
        indexes = {@Index(name = "idx_name", columnList = "username")
,@Index(name = "idx_createtime", columnList = "createtime")}
)
public class OperateLogEntity extends BaseEntity {
    private String username;
    private String operate;
    private Long time;
    private String ip;
    @Column(length = 2000)
    private String params;
    private String uri;
    private String type;

    @Transient
    private String usernameLike;
    @Transient
    private Date createTimeLt;
    @Transient
    private Date createTimeGt;

}
